﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AO.FireSimulation
{
	[CreateAssetMenu(fileName = "GameSettings", menuName = "App/GameSettings", order = 1)]
	public class GameSettings : ScriptableObject
	{
		[Header("Scene settings")]
		
		[Tooltip("Link to prefab of terrain")]
		public Terrain terrain;
		
		[Tooltip("Link to prefab of a tree")]
		public Tree treePrefab;
		
		[Tooltip("Base material of a tree")]
		public Material baseMaterial;
		
		[Tooltip("Burning material of a tree")]
		public Material burningMaterial;
		
		[Tooltip("Burnt material of a tree")]
		public Material burntMaterial;
		
		[Header("Simulation settings")]
		
		[Tooltip("Time in seconds of full tree burning")]
		public float timeToBurn = 2;
		
		[Tooltip("Minimum distance from burning tree to propagate fire on another tree")]
		public float minDist = 4;
		
		[Tooltip("Minimum time of tree burning to make possible fire propagation from it"), Range(0, 1)]
		public float minTimePercentToPropagate = 0.6f;
		
		[Tooltip("Maximum wind speed"), Range(0, 1)]
		public float windSpeed = 1;
		
		[Tooltip("Wind direction in angles"),Range(0, 360)]
		public float windDirection = 0;
		
		[Tooltip("Wind angle range"),Range(0, 90)]
		public float angleRange = 50;
		
		[Tooltip("Trees count"),Range(10, 2000)]
		public int treesCount = 200;
	}
}