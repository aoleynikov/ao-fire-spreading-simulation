﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AO.FireSimulation
{
    public class GameUI : MonoBehaviour
    {
        public Button generateTreesButton;
        public Button clearTreesButton;
        public Button simulateButton;
        public Button fireButton;
        public Button quitButton;
        
        public Toggle addModeToggle;
        public Toggle removeModeToggle;
        public Toggle fireModeToggle;
        
        public Slider windSpeedSlider;
        public Slider windDirectionSlider;
        public Slider treesCountSlider;

        public GameObject arrow;
    }
}