﻿using UnityEngine;

namespace AO.FireSimulation
{
    public class Tree : MonoBehaviour
    {
        [HideInInspector] public Vector3 position;
        [HideInInspector] public BurningState burningState;
        [HideInInspector] public float burnStartTime = 0;
        [HideInInspector] public float burningPercent = 0;
    }
}