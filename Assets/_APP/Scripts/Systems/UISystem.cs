﻿using System;
using Unity.Entities;
using UnityEngine;
using UnityEngine.UI;

namespace AO.FireSimulation
{
    public class UISystem: ComponentSystem
    {
        [Inject] private UIData _uiData;

        private GameSettings _gameSettings;

        private GameUI _gameUI;

        private bool _initialized = false;

        private Renderer _arrowRenderer;
        
        private Color _arrowNoWindColor = Color.white;
        private Color _arrowWindColor = Color.red;

        /// <summary>
        /// Initialize dependencies
        /// </summary>
        /// <param name="gameSettings"></param>
        public void Initialize(GameSettings gameSettings)
        {
            _gameSettings = gameSettings;
        }

        /// <summary>
        /// Setup all Ui elements callbacks
        /// </summary>
        private void SetupUI()
        {
            _gameUI = _uiData.UI[0];

            GameEvents.OnWindDirectionChanged += WindDirectionChanged;
            GameEvents.OnWindSpeedChanged += WindSpeedChanged;
            
            // setup toggles
            _gameUI.addModeToggle.isOn = true;
            _gameUI.fireModeToggle.isOn = false;
            _gameUI.removeModeToggle.isOn = false;
            
            // setup ui callbacks
            _gameUI.generateTreesButton.onClick.AddListener(delegate { GameEvents.OnGenerateTrees?.Invoke(); });
            _gameUI.clearTreesButton.onClick.AddListener(delegate { GameEvents.OnRemoveTrees?.Invoke(); });
            _gameUI.simulateButton.onClick.AddListener(delegate { GameEvents.OnStartSimulation?.Invoke(); });
            _gameUI.fireButton.onClick.AddListener(delegate { GameEvents.OnFireRandomTree?.Invoke(); });
            _gameUI.quitButton.onClick.AddListener(delegate { Application.Quit(); });
            
            _gameUI.windSpeedSlider.onValueChanged.AddListener(delegate { GameEvents.OnWindSpeedChanged?.Invoke(_uiData.UI[0].windSpeedSlider.value); });
            _gameUI.windDirectionSlider.onValueChanged.AddListener(delegate { GameEvents.OnWindDirectionChanged?.Invoke(_uiData.UI[0].windDirectionSlider.value); });
            _gameUI.treesCountSlider.onValueChanged.AddListener(delegate { _gameSettings.treesCount = (int)_gameUI.treesCountSlider.value; });
          
            _gameUI.addModeToggle.onValueChanged.AddListener(delegate { SetModeByToggles(_uiData.UI[0].addModeToggle, ModeType.Add); });
            _gameUI.fireModeToggle.onValueChanged.AddListener(delegate { SetModeByToggles(_uiData.UI[0].fireModeToggle, ModeType.Fire); });
            _gameUI.removeModeToggle.onValueChanged.AddListener(delegate { SetModeByToggles(_uiData.UI[0].removeModeToggle, ModeType.Remove); });

            _arrowRenderer = _gameUI.arrow.GetComponent<Renderer>();
            
            _initialized = true;
        }

        /// <summary>
        /// Rotate angle of wind
        /// </summary>
        /// <param name="direction"></param>
        private void WindDirectionChanged(float direction)
        {
            _gameSettings.windDirection = direction;
            _gameUI.arrow.transform.eulerAngles = new Vector3(90, 180 - direction, 0);
        }
        
        /// <summary>
        /// Color arrow of wind
        /// </summary>
        /// <param name="speed"></param>
        private void WindSpeedChanged(float speed)
        {
            _gameSettings.windSpeed = speed;
            _arrowRenderer.material.color = Color.Lerp(_arrowNoWindColor, _arrowWindColor, speed);
        }

        /// <summary>
        /// Modes of LMB
        /// </summary>
        /// <param name="toggle"></param>
        /// <param name="modeType"></param>
        private void SetModeByToggles(Toggle toggle, ModeType modeType)
        {
            if (toggle.isOn)
            {
                GameEvents.OnModeChanged?.Invoke(modeType);
            }
        }

        /// <summary>
        /// Update loop
        /// </summary>
        protected override void OnUpdate()
        {
            if (!_initialized && _uiData.Length > 0)
            {
                SetupUI();
            }
        }

    }
}