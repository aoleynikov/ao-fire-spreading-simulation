﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace AO.FireSimulation
{
	public class TreesGenerationSystem : ComponentSystem
	{
		private GameSettings _gameSettings;
		
		private Camera _camera;

		private Terrain _terrain;
		private TerrainData _terrainData;
		
		private GameObject _treeToDelete;
		private readonly List<Tree> _trees = new List<Tree>();
		private ModeType _modeType;

		
		/// <summary>
		/// Initialization
		/// </summary>
		/// <param name="gameSettings"></param>
		/// <param name="camera"></param>
		public void Initialize(GameSettings gameSettings, Camera camera)
		{
			_gameSettings = gameSettings;
			_camera = camera;

			_terrain = GameObject.Instantiate(_gameSettings.terrain);
			_terrainData = _terrain.terrainData;

			GameEvents.OnGenerateTrees += GenerateTrees;
			GameEvents.OnRemoveTrees += RemoveAllTrees;
			GameEvents.OnModeChanged += ModeChanged;
		}
		
		/// <summary>
		/// Generate trees
		/// </summary>
		private void GenerateTrees()
		{
			RemoveAllTrees();

			for (int i = 0; i < _gameSettings.treesCount; i++)
			{
				AddRandomTree();
			}
		}
		
		/// <summary>
		/// Add random tree
		/// </summary>
		private void AddRandomTree()
		{
			Vector3 treePosition = new Vector3(Random.Range(0.0f, 1.0f), 0, Random.Range(-0.0f, 1.0f));
			treePosition.y = _terrain.terrainData.GetInterpolatedHeight(treePosition.x, treePosition.z);

			treePosition.x = treePosition.x * _terrainData.size.x;
			treePosition.z = treePosition.z * _terrainData.size.z;

			AddTreeAtPosition(treePosition);
		}

		/// <summary>
		/// Add tree at position
		/// </summary>
		/// <param name="position"></param>
		/// <returns></returns>
		private Tree AddTreeAtPosition(Vector3 position)
		{
			Tree tree = GameObject.Instantiate(_gameSettings.treePrefab);
			tree.transform.position = position;
			tree.position = position;
			
			_trees.Add(tree);

			return tree;
		}

		/// <summary>
		/// Remove all trees
		/// </summary>
		private void RemoveAllTrees()
		{
			for (int i = _trees.Count - 1; i >= 0; i--)
			{
				if (_trees[i] != null)
				{
					GameObject.Destroy(_trees[i].gameObject);
				}
			}

			_trees.Clear();
		}

		/// <summary>
		/// Mode of LMB changed
		/// </summary>
		/// <param name="modeType"></param>
		private void ModeChanged(ModeType modeType)
		{
			_modeType = modeType;
		}
		
		/// <summary>
		/// Update loop — checks for LMB click on terrain / trees and delete trees
		/// </summary>
		protected override void OnUpdate()
		{
			if (Input.GetMouseButtonDown(0))
			{
				Ray ray = _camera.ScreenPointToRay(Input.mousePosition);

				RaycastHit hit;
				if (Physics.Raycast(ray, out hit))
				{
					if (hit.transform.gameObject.CompareTag("Terrain"))
					{
						if (_modeType == ModeType.Add)
						{
							// add new tree
							Tree tree = AddTreeAtPosition(hit.point);
							GameEvents.OnAddConcreteTree?.Invoke(tree);
						}
					}
					else if (hit.transform.gameObject.CompareTag("Tree"))
					{
						if (_modeType == ModeType.Remove)
						{
							// delete tree
							_treeToDelete = hit.transform.gameObject;
						}
						else if (_modeType == ModeType.Fire)
						{
							// lights / extinguishes fire on tree
							Tree tree = hit.transform.gameObject.GetComponent<Tree>();
							GameEvents.OnChangeBurningState?.Invoke(tree);
						}
					}
				}
			}
			
			if (_treeToDelete != null)
			{
				for (int i = _trees.Count - 1; i >= 0; i--)
				{
					if (_trees[i].gameObject == _treeToDelete)
					{
						_trees.RemoveAt(i);
					}
				}
				
				GameEvents.OnRemoveConcreteTree?.Invoke(_treeToDelete.GetComponent<Tree>());
					
				GameObject.Destroy(_treeToDelete);

				_treeToDelete = null;
			}
		}

		/// <summary>
		/// Dispose all
		/// </summary>
		public void Dispose()
		{
			// unsubscribe from events
			GameEvents.OnGenerateTrees -= GenerateTrees;
			GameEvents.OnRemoveTrees -= RemoveAllTrees;
			GameEvents.OnModeChanged -= ModeChanged;
		}

	}
}