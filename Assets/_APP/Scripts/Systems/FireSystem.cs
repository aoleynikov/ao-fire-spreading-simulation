﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Unity.Entities;
using Unity.Transforms2D;
using UnityEngine;
using UnityEngine.Events;
using Random = UnityEngine.Random;

namespace AO.FireSimulation
{
	public class FireSystem : ComponentSystem
	{
		[Inject] private TreesData _treesData;

		private GameSettings _gameSettings;
		
		private readonly List<Tree> _allTreesList = new List<Tree>();
		private readonly List<Tree> _burningTrees = new List<Tree>();
		
		private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
		private CancellationToken _cancellationToken;

		/// <summary>
		/// Initialization
		/// </summary>
		/// <param name="gameSettings"></param>
		/// <param name="camera"></param>
		public void Initialize(GameSettings gameSettings, Camera camera)
		{
			_gameSettings = gameSettings;
			
			_cancellationToken = _cancellationTokenSource.Token;
			
			GameEvents.OnStartSimulation += StartSimulation;
			GameEvents.OnFireRandomTree += FireSeveralRandomTrees;
			GameEvents.OnGenerateTrees += RemoveTrees;
			GameEvents.OnRemoveTrees += RemoveTrees;
			GameEvents.OnRemoveConcreteTree += RemoveConcreteTree;
			GameEvents.OnAddConcreteTree += AddConcreteTree;
			GameEvents.OnChangeBurningState += ChangeBurningState;
			
			BurnLoop();
		}
		
		/// <summary>
		/// Main loop of burning
		/// </summary>
		private async void BurnLoop()
		{
			while (true)
			{
				// get all trees that should be burn
				List<Tree> treesToBurn = await Task.Run(() => GetTreesToBurn());
				
				foreach (Tree tree in treesToBurn)
				{
					_burningTrees.Add(tree);
					SetTreeBurningState(tree, BurningState.Burning);
					tree.burnStartTime = Time.time;
				}
				
				await Task.Delay(TimeSpan.FromSeconds(0.001f));
				
				if (_cancellationToken.IsCancellationRequested)
				{
					break;
				}
			}
		}
		
		/// <summary>
		/// Checks for all the trees that need to be burn
		/// </summary>
		/// <returns></returns>
		private List<Tree> GetTreesToBurn()
		{
			List<Tree> treesToBurn = new List<Tree>();
		
			for (int i = 0; i < _burningTrees.Count; i++)
			{
				if (_burningTrees[i].burningPercent >= _gameSettings.minTimePercentToPropagate)
				{
					// if tree burning time is too small — this tree cannot burn another
					if (_burningTrees[i].burningState != BurningState.Burning)
						continue;

					for (int j = 0; j < _allTreesList.Count; j++)
					{
						if (_allTreesList[j].burningState == BurningState.Base)
						{
							//this distance evaluation solution is faster than calculating Vector3.Distance
							float xD = _burningTrees[i].position.x - _allTreesList[j].position.x;
							float yD = _burningTrees[i].position.y - _allTreesList[j].position.y;
							float zD = _burningTrees[i].position.z - _allTreesList[j].position.z;
							float dist2 = xD * xD + yD * yD + zD * zD;

							float checkDist = _gameSettings.minDist * _gameSettings.minDist * 
							                  _gameSettings.windSpeed * _gameSettings.windSpeed;
							
							// smaller burning time of tree affects on smaller distance around this tree
							// in which tree can burn another
							if (dist2 < checkDist * _burningTrees[i].burningPercent)
							{
								float xDiff = _burningTrees[i].position.x - _allTreesList[j].position.x;
								float zDiff = _burningTrees[i].position.z - _allTreesList[j].position.z;
								float angle = Mathf.Atan2(zDiff, xDiff) * 180.0f / Mathf.PI;

								if (angle < 0)
									angle = 360 + angle;

								if (Mathf.Abs(angle - _gameSettings.windDirection) < _gameSettings.angleRange)
								{
									treesToBurn.Add(_allTreesList[j]);
									_allTreesList[j].burningState = BurningState.Burning;
								}
							}
						}
					}
				}
			}
			
			return treesToBurn;
		}

		/// <summary>
		/// Initialize all lists and fill them with new generated trees
		/// </summary>
		private void FillData()
		{
			RemoveTrees();
			
			for (int i = 0; i < _treesData.Length; i++)
			{
				Tree tree = _treesData.Trees[i];
			
				SetTreeBurningState(tree, BurningState.Base);
			
				_allTreesList.Add(tree);
			}

		}
		
		/// <summary>
		/// Picks random tree and starts simulation
		/// </summary>
		private void StartSimulation()
		{
			FillData();

			FireRandomTree();
		}

		/// <summary>
		/// Fires random tree
		/// </summary>
		private void FireRandomTree()
		{
			int randomTreeID = Random.Range(0, _allTreesList.Count);

			Tree randomTree = _allTreesList[randomTreeID];
			randomTree.burnStartTime = Time.time;
			randomTree.burningState = BurningState.Burning;
			randomTree.gameObject.GetComponent<Renderer>().material = _gameSettings.burningMaterial;

			_burningTrees.Add(randomTree);
		}

		/// <summary>
		/// Fires several random trees
		/// </summary>
		private void FireSeveralRandomTrees()
		{
			for (int i = 0; i < 3; i++)
			{
				FireRandomTree();
			}
		}
		
		/// <summary>
		/// Removes all trees
		/// </summary>
		private void RemoveTrees()
		{
			_allTreesList.Clear();
			_burningTrees.Clear();
		}
		
		/// <summary>
		/// Sets tree as burning
		/// </summary>
		/// <param name="tree"></param>
		private void AddTreeToBurning(Tree tree)
		{
			_burningTrees.Add(tree);
		}

		/// <summary>
		/// Removes tree from burning
		/// </summary>
		/// <param name="treeToDelete"></param>
		private void RemoveTreeFromBurning(Tree treeToDelete)
		{
			for (int i = _burningTrees.Count - 1; i >= 0; i--)
			{
				if (_burningTrees[i] == treeToDelete)
				{
					_burningTrees.RemoveAt(i);
				}
			}
		}

		/// <summary>
		/// Removes tree from all lists
		/// </summary>
		/// <param name="treeToDelete"></param>
		private void RemoveTreeFromAll(Tree treeToDelete)
		{
			for (int i = _allTreesList.Count - 1; i >= 0; i--)
			{
				if (_allTreesList[i] == treeToDelete)
				{
					_allTreesList.RemoveAt(i);
				}
			}
		}

		/// <summary>
		/// Removes concrete tree from burning and all trees lists
		/// </summary>
		/// <param name="treeToDelete"></param>
		private void RemoveConcreteTree(Tree treeToDelete)
		{
			RemoveTreeFromBurning(treeToDelete);
			RemoveTreeFromAll(treeToDelete);
		}
		
		/// <summary>
		/// Adds new tree to all trees list
		/// </summary>
		/// <param name="treeToAdd"></param>
		private void AddConcreteTree(Tree treeToAdd)
		{
			_allTreesList.Add(treeToAdd);
		}

		/// <summary>
		/// Changes burning states of a tree (for LBM click)
		/// </summary>
		/// <param name="tree"></param>
		private void ChangeBurningState(Tree tree)
		{
			if (tree.burningState == BurningState.Burning)
			{
				SetTreeBurningState(tree, BurningState.Base);
				
				tree.burningPercent = 0;
				tree.burnStartTime = 0;

				RemoveTreeFromBurning(tree);
			}
			else if (tree.burningState == BurningState.Base)
			{
				SetTreeBurningState(tree, BurningState.Burning);
				
				tree.burningPercent = 0;
				tree.burnStartTime = Time.time;
				
				AddTreeToBurning(tree);
			}
		}

		
		/// <summary>
		/// Changes a look and a state of a tree
		/// </summary>
		/// <param name="tree"></param>
		/// <param name="burningState"></param>
		private void SetTreeBurningState(Tree tree, BurningState burningState)
		{
			tree.burningState = burningState;

			if (burningState == BurningState.Base)
			{
				tree.gameObject.GetComponent<Renderer>().material = _gameSettings.baseMaterial;
			}
			else if (burningState == BurningState.Burning)
			{
				tree.gameObject.GetComponent<Renderer>().material = _gameSettings.burningMaterial;
			}
			else if (burningState == BurningState.Burnt)
			{
				tree.gameObject.GetComponent<Renderer>().material = _gameSettings.burntMaterial;
			}
		}

		/// <summary>
		/// Update loop — changes burning time of trees
		/// </summary>
		protected override void OnUpdate()
		{
			for (int i = _burningTrees.Count - 1; i >= 0; i--)
			{
				if (_burningTrees[i] == null)
					continue;				
				
				if (_burningTrees[i].burningState == BurningState.Burning)
				{
					_burningTrees[i].burningPercent = (Time.time - _burningTrees[i].burnStartTime) / _gameSettings.timeToBurn;
				}
				
				if (_burningTrees[i].burningState == BurningState.Burning && _burningTrees[i].burningPercent >= 1.0f)
				{
					_burningTrees[i].burningState = BurningState.Burnt;
					
					_burningTrees[i].gameObject.GetComponent<Renderer>().material = _gameSettings.burntMaterial;
				}
			}
		}

		/// <summary>
		/// Dispose all
		/// </summary>
		public void Dispose()
		{
			// unsubscribe from events
			GameEvents.OnStartSimulation -= StartSimulation;
			GameEvents.OnFireRandomTree -= FireSeveralRandomTrees;
			GameEvents.OnGenerateTrees -= RemoveTrees;
			GameEvents.OnRemoveTrees -= RemoveTrees;
			GameEvents.OnRemoveConcreteTree -= RemoveConcreteTree;
			GameEvents.OnAddConcreteTree -= AddConcreteTree;
			GameEvents.OnChangeBurningState -= ChangeBurningState;
			
			// stop thread
			_cancellationTokenSource.Cancel();
			
		}
	}
}