﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AO.FireSimulation
{
	public class GameEvents : MonoBehaviour
	{
		public static UnityAction OnGenerateTrees;
		public static UnityAction OnRemoveTrees;
		public static UnityAction OnStartSimulation;
		public static UnityAction OnFireRandomTree;
		
		public static UnityAction<Tree> OnRemoveConcreteTree;
		public static UnityAction<Tree> OnAddConcreteTree;
		public static UnityAction<Tree> OnChangeBurningState;

		public static UnityAction<float> OnWindSpeedChanged;
		public static UnityAction<float> OnWindDirectionChanged;

		public static UnityAction<ModeType> OnModeChanged;
	}
}