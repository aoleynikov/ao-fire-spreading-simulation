﻿namespace AO.FireSimulation
{
    public enum ModeType
    {
        Add,
        Remove,
        Fire
    }
}