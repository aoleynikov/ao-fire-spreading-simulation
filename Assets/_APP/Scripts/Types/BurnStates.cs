﻿namespace AO.FireSimulation
{
    public enum BurningState
    {
        Base,
        Burning,
        Burnt
    }
}