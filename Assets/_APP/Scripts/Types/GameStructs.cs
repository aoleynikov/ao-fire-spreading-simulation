﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;
using UnityEngine.EventSystems;

namespace AO.FireSimulation
{
	public struct TreesData
	{
		public int Length;
		public ComponentArray<Tree> Trees;
	}

	public struct UIData
	{
		public int Length;
		public ComponentArray<GameUI> UI;
	}
}