﻿using System.Collections;
using System.Collections.Generic;
using Unity.Entities;
using UnityEngine;

namespace AO.FireSimulation
{
	public class FireSpreadBootstrap : MonoBehaviour
	{
		[SerializeField]
		private Camera _camera;
		
		private GameSettings gameSettings;
		
		private TreesGenerationSystem _treesGenerationSystem;
		private UISystem _uiSystem;
		private FireSystem _fireSystem;
	
		private void Start()
		{
			gameSettings = Resources.Load("GameSettings") as GameSettings;

			if (!gameSettings)
				return;
			
			_treesGenerationSystem = World.Active.GetOrCreateManager<TreesGenerationSystem>();
			_treesGenerationSystem.Initialize(gameSettings, _camera);
	
			_uiSystem = World.Active.GetOrCreateManager<UISystem>();
			_uiSystem.Initialize(gameSettings);
			
			_fireSystem = World.Active.GetOrCreateManager<FireSystem>();
			_fireSystem.Initialize(gameSettings, _camera);
		}

		private void OnDisable()
		{
			_fireSystem.Dispose();
			_treesGenerationSystem.Dispose();
		}
	}
}
